# Icons

[![pipeline status](https://gitlab.com/coyoapp/icons/badges/master/pipeline.svg)](https://gitlab.com/coyoapp/icons/commits/master)

This project contains all icons of COYO's custom SVG icon set. If you want to add a new icon to this repository,
please make sure that your SVG icon conforms with the overall structure of the other SVG icons:

    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="{{ICON_PATH}}"/>
    </svg>

## Package management

This project is using [npm](https://www.npmjs.com/) as a package manager. This is especially important if you want to
add new dependencies to a project and fail to provide an updated `package-lock.json`. This will cause the CI build to
fail. If dependencies in the package lock do not match those in `package.json`, npm will exit with an error in the CI
build, instead of updating the package lock.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist` directory. SVG sprites and
style definitions for different preprocessors will automatically be generated in `dist/style` and `dist/svg`
respectively. Also, standalone HTML previews will become available in the `dist` directory.

## Publishing

The library can be published by running `npm publish`. However, the preferred way to publish a new version of the
library is via the CI setup. A new version of the library will automatically be published to our
[JFrog Artifactory](https://mindsmash.jfrog.io/mindsmash) when pushing a new tag on the master branch. To simplyfy
this process, you can use the following npm commands:

 * **npm run release:major** releases a new *major* version
 * **npm run release:minor** releases a new *minor* version
 * **npm run release:patch** releases a new *patch* version

These commands will automatically increase the version number in the `package.json` and `package-lock.json`, commit
the changed files, create a corresponding git tag and push everything to the remote branch. This will then
automatically trigger an update of https://coyoapp.gitlab.io/icons and publish a new library version.

## Adding icons via the browser

1. Go to [Repository > Files > assets](https://gitlab.com/coyoapp/icons/tree/master/assets) and upload your new icon
   to the `assets` directory.

   ![](images/web_1.png)
2. Upload the files to the `master` branch and add a commit message.

   ![](images/web_2.png)

3. Navigate to [CI/CD > Pipelines](https://gitlab.com/coyoapp/icons/pipelines) and find the pipeline triggered by your
   recent commit. Than run the manual `version` job to automatically trigger the build and publish jobs for a new
   patch release.

   ![](images/web_3.png)
